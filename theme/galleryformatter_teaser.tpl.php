<?php
/**
 * @file
 * Template file for the galleryformatter_teaser default formatter
 */

/**
 * This template provides a basic teaser with the first 5 thumbnails of the
 * gallery formatter being displayed at the bottom of the teaser. Clicking on
 * any of the teaser elements takes you to the node, and also to the appropriate
 * image in the node.
 *
 * This is heavily dependent on the processing in galleryformatter
 *
 *
 * Available variables:
 *
 * $gallery_slide_height
 * $gallery_slide_width
 * $gallery_thumb_height
 * $gallery_thumb_width
 * $gallery_slides - Array containing all slide images, a link to the original and its sanatized title & description ready to print
 * $gallery_thumbs - Array containing all thumbnail images ready to print
 * $link_to_full -  BOOLEAN wether or not we are linking slides to original images
 */
?>
<?php $count = 0; ?>
<?php if($gallery_thumbs): ?>
<div class="gallery-thumbs">
  <div class="wrapper">
    <ul>
      <?php foreach ($gallery_thumbs as $id => $data): ?>
      <a href="<?php print $element['#node']->path; ?>#<?php print $data['hash_id']; ?>"><?php print $data['image']; ?></a>
      <?php $count++; ?>
      <?php if ($count >= 5) {break;} ?>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<?php endif; ?>
